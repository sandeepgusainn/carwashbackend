# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

--> To run test on localhost -- 
* Clone application in your local enviroment.
* Install all the dependencies using "npm install".
* After installing dependencies successfully run the code using "npm run start"

--> To run test on prod server -- 
* Clone application in your server.
* Install all the dependencies using "npm install".
* After installing dependencies successfully, run this to build dist folder "npm run build"
* after building dist, run the code to start the server "npm run start:prod"
* Run the code to reload/restart the server "npm run reload:prod"

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact