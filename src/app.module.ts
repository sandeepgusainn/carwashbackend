import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

// Module created for the project
import { memberModule } from './B2B/members/member.module';
import { AuthModule } from './auth/auth.module';
import { companyModule } from './B2B/company/company.module';
import { addonModule } from './B2B/addon/addon.module';
import { addressModule } from './B2B/address/address.module';
import { cardetailModule } from './B2B/cardetails/cardetail.module';
import { requestModule } from './B2B/washrequest/request.module';
import { productModule } from './B2B/products/product.module';

// Global service file
import { GlobalServiceService } from './global-service/global-service.service';

// Module required for the project
import { MongooseModule } from '@nestjs/mongoose';
import { I18nModule, I18nJsonParser } from 'nestjs-i18n';
import * as path from 'path';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot('mongodb://localhost/carwashdev', { useNewUrlParser: true }),
    I18nModule.forRoot({
      fallbackLanguage: 'en',
      parser: I18nJsonParser,
      parserOptions: {
        path: path.join(__dirname, '/i18n/'),
        watch: true,
      },
    }),
    AuthModule,
    memberModule,
    companyModule,
    addonModule,
    addressModule,
    cardetailModule,
    requestModule,
    productModule
  ],
  controllers: [AppController],
  providers: [AppService, GlobalServiceService],
})
export class AppModule { }
