import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { product, productClass } from './product.schema';
import { GlobalServiceService } from '../../global-service/global-service.service';

@Injectable()
export class productService {
    constructor(
        @InjectModel(product.name) private productModel: Model<product>,
        private readonly global: GlobalServiceService,
    ) {
    }

    async create(): Promise<product[]> {
        const newproductList1 = this.global.getProductList();
        const productList1 = this.productModel.find({});
        const [newproductList, productList] = await Promise.all([newproductList1, productList1])
        console.log("productService -> newproductList, productList", newproductList, productList)
        if (productList.length) {
            const result = newproductList.data.filter(function (o1) {
                // filter out (!) items in newproductList
                return !productList.some(function (o2) {
                    if (o1.id === o2.productId) {         //  unique id - productId
                        o2.isActive = true
                        o2.name = o1.name
                        return o2.save();
                    }
                });
            })
            return result.forEach(async element => {
                const product = new this.productModel()
                product.name = element.name
                product.productId = element.id
                await product.save()
            });
        } else {
            const produtsave = newproductList.data.forEach(async element => {
                const product = new this.productModel()
                product.name = element.name
                product.productId = element.id
                await product.save()
            });
            return produtsave;
        }

    }

    async getList(): Promise<product[]> {
        return this.productModel.find({ isActive: true }).exec();
    }
}
