import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
    timestamps: true
})
export class product extends Document {

    @Prop({ required: true })
    name: string;
 
    @Prop({ required: true })
    productId: string;

    // @Prop({ required: true })
    // priceId: string;

    // @Prop({})
    // price: string;
    
    // @Prop({})
    // description: string;

    @Prop({ default: true })
    isActive: boolean;
}

export const productSchema = SchemaFactory.createForClass(product);

// db.products.insertMany([{"name": "Spray"}, {"name":"Shampoing sièges en cuir"},{"name": "Shampoing sièges en tissus"},{"name": "Shampoing tapis et coffre"},{"name": "Polissage carrosserie machine"},{"name": "Simonisage carrosserie à la main"},{"name": "Disinfectant"},{"name": "Véhicule très sale"},{"name": "Nettoyage poils d'animaux"}])

export class productClass {
    readonly _id: string;
    readonly name: string;
    readonly price: string;
    readonly description: string;
    readonly isActive: boolean;
}
