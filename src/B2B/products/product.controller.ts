/* eslint-disable @typescript-eslint/no-unused-vars */
import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { productService } from './product.service';
import { product, productClass } from './product.schema'
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';

@Controller('product')
export class productController {
    constructor(
        private readonly productservice: productService,
    ) { }

    @UseGuards(JwtAuthGuard)
    @Post()
    async create(@Request() req): Promise<string> {
        console.log(req.user);
        this.productservice.create();
        return 'Products updated successfully'
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    async getProfile(@Request() req): Promise<product[]> {
        console.log(req.user);
        return this.productservice.getList();
    }
}
