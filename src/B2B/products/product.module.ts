import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { productController } from './product.controller';
import { productService } from './product.service';
import { product, productSchema } from './product.schema';
import { GlobalServiceService } from '../../global-service/global-service.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: product.name, schema: productSchema }]),
    ],
    controllers: [productController],
    providers: [productService, GlobalServiceService],
    exports: [productService],
})
export class productModule { }