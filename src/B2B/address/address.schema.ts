import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
    timestamps: true
})
export class address extends Document {

    @Prop({ ref: 'user' })
    user: string;

    @Prop({ ref: 'user' })
    createdBy: string;

    @Prop({ ref: 'company' })
    companyId: string;

    @Prop({})
    name: string;

    @Prop({})
    landMark: string;

    @Prop({})
    address: object;

    // {
    //     street_number: string,
    //     route: string,
    //     locality: string,
    //     administrative_area_level_1: string,
    //     country: string,
    //     postal_code: string
    // }

    @Prop({ default: true })
    isActive: boolean;
}

export const addressSchema = SchemaFactory.createForClass(address);

export class addressClass {
    readonly _id: string;
    readonly user: string;
    readonly admin: string;
    readonly landMark: string;
    readonly name: string;
    readonly isActive: boolean;
    readonly address;
}
