import { Model } from 'mongoose';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { address, addressClass } from './address.schema';
import { serveadd, serveaddClass } from './addressServe.schema';
import { user } from '../members/user.schema';
import { allowancegroup } from '../members/allowancegroup.schema';
import { promises } from 'dns';

@Injectable()
export class addressService {
    constructor(
        @InjectModel(address.name) private addressModel: Model<address>,
        @InjectModel(serveadd.name) private serveaddModel: Model<serveadd>,
        @InjectModel(user.name) private userModel: Model<user>,
        @InjectModel(allowancegroup.name) private allowancegroupModel: Model<allowancegroup>,
    ) {
    }

    // add new address and check if it comes in our serve zone
    async addAddress(address: addressClass, user): Promise<address | object> {
        const isserveadd = await this.serveaddModel.findOne({ code: address.address.postal_code, country: address.address.country }).exec()
        if (isserveadd) {
            const addres = new this.addressModel(address);
            addres.createdBy = user.userId; // created by admin, moderator or employee
            const user1 = await this.userModel.findOne({ _id: user.userId }).exec()
            addres.companyId = user1.companyId; // added main admin of a particular company account
            return { data: await addres.save() };
        } else {
            throw new BadRequestException({ statusCode: 400, message: "Sorry, We don't serve in this location right now" });
        }
    }

    // edit above address
    async editAddress(data, userId): Promise<address | object> {
        const isserveadd = await this.serveaddModel.findOne({ code: data.address.postal_code, country: data.address.country }).exec()
        if (isserveadd) {
            const addres = await this.addressModel.findOne({ $or: [{ _id: data.id, user: userId }, { _id: data.id, createdBy: userId }] }).exec();
            addres.address = data.address;
            return { data: await addres.save() };
        } else {
            throw new BadRequestException({ statusCode: 400, message: "Sorry, We don't serve in this location right now" });
        }
    }

    // get added address list added by user
    async getAddressList(user): Promise<object> {
        if (user.role === 'admin' || user.role === 'moderator') {
            const user1 = await this.userModel.findOne({ _id: user.userId }).exec()
            return { data: await this.addressModel.find({ companyId: user1.companyId }).exec() };
        } else if (user.role === 'allowance manager') {
            const groupManager = await this.allowancegroupModel.findOne({ groupadmin: user.userId }).exec()
            let addressList = [];
            const dat = groupManager.memberList.map(async elem => {
                const useradd = await this.addressModel.find({ $or: [{ user: elem }, { createdBy: elem }] }).exec()
                addressList.push(useradd)
            });
            await Promise.all([dat])
            return { data: addressList };
        } else {
            return { data: await this.addressModel.find({ $or: [{ user: user.userId }, { createdBy: user.userId }] }).exec() };
        }
    }

    // get particular address list added by user
    async getAddress(id): Promise<object> {
        return { data: await this.addressModel.findOne({ _id: id }).exec() };
    }


    //  add and get address where city this product serve
    async addServeAddress(serveadd: serveaddClass): Promise<object> {
        console.log("addressService -> serveadd", serveadd)
        const addres = new this.serveaddModel(serveadd);
        console.log("addressService -> addres", addres)
        return { data: await addres.save() };
    }
    async getserveaddList(): Promise<object> {
        return { data: await this.serveaddModel.find({}).exec() };
    }
}
