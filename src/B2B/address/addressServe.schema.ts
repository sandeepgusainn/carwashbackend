import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
    timestamps: true
})
export class serveadd extends Document {

    @Prop()
    country: string;

    @Prop()
    city: string;

    @Prop()
    code: string;

    @Prop({ default: true })
    isActive: boolean;
}

export const serveaddSchema = SchemaFactory.createForClass(serveadd);

export class serveaddClass {
    readonly _id: string;
    readonly country: string;
    readonly city: string;
    readonly code: string;
    readonly isActive: boolean;
}
