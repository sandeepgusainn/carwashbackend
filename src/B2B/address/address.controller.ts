/* eslint-disable @typescript-eslint/no-unused-vars */
import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { addressService } from './address.service';
import { address, addressClass } from './address.schema'
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { serveadd, serveaddClass } from './addressServe.schema'

@Controller('address')
export class addressController {
    constructor(
        private readonly addressservice: addressService,
    ) { }

    // add new address and check if it comes in our serve zone
    @UseGuards(JwtAuthGuard)
    @Post()
    async addAddress(@Body() addressClass: addressClass, @Request() req): Promise<address | object> {
        return this.addressservice.addAddress(addressClass, req.user);
    }

    // edit above address
    @UseGuards(JwtAuthGuard)
    @Post('edit')
    async editAddress(@Request() req): Promise<address | object> {
        return this.addressservice.editAddress(req.body, req.user.userId);
    }

    // get added address list added by user
    @UseGuards(JwtAuthGuard)
    @Get('addresslist')
    async getAddressList(@Request() req): Promise<object> {
        return this.addressservice.getAddressList(req.user);
    }

    // get particular address list added by user
    @UseGuards(JwtAuthGuard)
    @Get()
    async getAddress(@Request() req): Promise<object> {
        console.log(req.user);
        return this.addressservice.getAddress(req.query.id);
    }

    //  add and get address where city this product serve
    @Post('addserveaddress')
    async addServeAddress(@Body() serveaddClass: serveaddClass): Promise<object> {
        console.log("addressController -> serveaddClass", serveaddClass)
        return this.addressservice.addServeAddress(serveaddClass);
    }

    @Get('serveaddress')
    async getServeAddress(): Promise<object> {
        return this.addressservice.getserveaddList();
    }

}
