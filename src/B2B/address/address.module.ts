import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { addressController } from './address.controller';
import { addressService } from './address.service';
import { address, addressSchema } from './address.schema';
import { serveadd, serveaddSchema } from './addressServe.schema';
import { user, userSchema } from '../members/user.schema';
import { allowancegroup, allowancegroupSchema } from '../members/allowancegroup.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: address.name, schema: addressSchema }]),
        MongooseModule.forFeature([{ name: serveadd.name, schema: serveaddSchema }]),
        MongooseModule.forFeature([{ name: user.name, schema: userSchema }]),
        MongooseModule.forFeature([{ name: allowancegroup.name, schema: allowancegroupSchema }]),
    ],
    controllers: [addressController],
    providers: [addressService],
    exports: [addressService],
})
export class addressModule { }