import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
    timestamps: true
})
export class addon extends Document {

    @Prop({ required: true })
    name: string;

    @Prop({})
    price: string;

    @Prop({})
    description: string;

    @Prop({ ref: 'product' })
    productId: string;

    @Prop({})
    priceId: string;

    @Prop({ default: true })
    isActive: boolean;
}

export const addonSchema = SchemaFactory.createForClass(addon);

// db.addons.insertMany([{"name": "Spray"}, {"name":"Shampoing sièges en cuir"},{"name": "Shampoing sièges en tissus"},{"name": "Shampoing tapis et coffre"},{"name": "Polissage carrosserie machine"},{"name": "Simonisage carrosserie à la main"},{"name": "Disinfectant"},{"name": "Véhicule très sale"},{"name": "Nettoyage poils d'animaux"}])

export class addonClass {
    readonly _id: string;
    readonly name: string;
    readonly price: string;
    readonly description: string;
    readonly productId: string;
    readonly priceId: string;
    readonly isActive: boolean;
}
