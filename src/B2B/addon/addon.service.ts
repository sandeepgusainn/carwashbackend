import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { addon, addonClass } from './addon.schema';
import { GlobalServiceService } from '../../global-service/global-service.service';

@Injectable()
export class addonService {
    constructor(
        @InjectModel(addon.name) private addonModel: Model<addon>,
        private readonly global: GlobalServiceService
    ) {
    }

    async create(addon: addonClass): Promise<object> {
        const addons = new this.addonModel(addon);
        const price = await this.global.getNumberWash(addon.productId);
        addons.priceId = price.id;
        addons.price = price.id;
        return { data: await addons.save()};
    }

    async getList(): Promise<object> {
        return { data: await this.addonModel.find({ isActive: true }).exec()};
    }
}
