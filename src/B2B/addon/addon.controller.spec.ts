import { Test, TestingModule } from '@nestjs/testing';
import { addonController } from './addon.controller';

describe('addon Controller', () => {
  let controller: addonController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [addonController],
    }).compile();

    controller = module.get<addonController>(addonController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
