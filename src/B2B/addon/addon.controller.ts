/* eslint-disable @typescript-eslint/no-unused-vars */
import { Body, Controller, Get, Post, Request, UseGuards, UnauthorizedException } from '@nestjs/common';
import { addonService } from './addon.service';
import { addon, addonClass } from './addon.schema'
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';

@Controller('addon')
export class addonController {
    constructor(
        private readonly addonservice: addonService,
    ) { }

    @Post()
    async create(@Request() req, @Body() addonClass: addonClass): Promise<object> {
        if (req.user.role === 'admin' || req.user.role === 'moderator') {
            return this.addonservice.create(addonClass);
        } else {
            throw new UnauthorizedException({ statusCode: 401, message: 'Unauthorized' });
        }
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    async getProfile(): Promise<object> {
        return this.addonservice.getList();
    }
}
