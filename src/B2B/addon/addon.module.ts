import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { addonController } from './addon.controller';
import { addonService } from './addon.service';
import { addon, addonSchema } from './addon.schema';
import { GlobalServiceService } from '../../global-service/global-service.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: addon.name, schema: addonSchema }]),
    ],
    controllers: [addonController],
    providers: [addonService, GlobalServiceService],
    exports: [addonService],
})
export class addonModule { }