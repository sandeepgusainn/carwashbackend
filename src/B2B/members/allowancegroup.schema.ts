import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
    timestamps: true
})
export class allowancegroup extends Document {

    @Prop({ ref: 'user' })
    groupadmin: string;

    @Prop({ ref: 'user' })
    createdBy: string;

    @Prop({ ref: 'user' })
    memberList: [];

    @Prop({ required: true })
    name: string;

    @Prop({ required: true })
    totalWashPerMonth: string;

    @Prop({ default: false })
    addOn: boolean;

    @Prop({ default: false })
    premiumwash: boolean;

    @Prop({ default: true })
    isActive: boolean;

    @Prop({ default: true })
    isEdit: boolean;

    @Prop({ default: true })
    isDelete: boolean;

    @Prop({ ref: 'company' })
    companyId: string;
}

export const allowancegroupSchema = SchemaFactory.createForClass(allowancegroup);

export class allowancegroupClass {
    readonly admin: string;
    readonly _id: string;
    readonly groupadmin: string;
    readonly name: string;
    readonly totalWashPerMonth: string;
    readonly addOn: boolean;
    readonly premiumwash: boolean;
    readonly createdBy: string;
    readonly companyId: string;
    readonly memberList;
    readonly isActive: boolean;
}