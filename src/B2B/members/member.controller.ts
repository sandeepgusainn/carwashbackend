/* eslint-disable @typescript-eslint/no-unused-vars */
import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { memberService } from './member.service';
import { user, userClass } from './user.schema'
import { allowancegroup, allowancegroupClass } from './allowancegroup.schema'
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
// import { GlobalServiceService } from '../../global-service/global-service.service';

@Controller('user')
export class memberController {
    constructor(
        private readonly memberservice: memberService,
        // private readonly global: GlobalServiceService
    ) { }

    @Post('changepassword')
    async changePassword(@Request() req): Promise<userClass | object> {
        return this.memberservice.findOneAndChangePassword(req)
    }

    @UseGuards(JwtAuthGuard)
    @Post('editprofile')
    async editProfile(@Request() req): Promise<userClass | object> {
        return this.memberservice.editProfile(req.body, req.user);
    }

    @UseGuards(JwtAuthGuard)
    @Get('profile')
    async getProfile(@Request() req): Promise<object> {
        return this.memberservice.getProfile(req.user.email);
    }

    @UseGuards(JwtAuthGuard)
    @Post('delete')
    async deleteProfile(@Request() req): Promise<object> {
        return this.memberservice.deleteProfile(req.user, req.body.id);
    }

    @UseGuards(JwtAuthGuard)
    @Get('userlist')
    async userList(@Request() req): Promise<object> {
        return this.memberservice.userList(req.user);
    }

    @UseGuards(JwtAuthGuard)
    @Get('allowancegrouplist')
    async allowanceGroupList(@Request() req): Promise<object> {
        return this.memberservice.allowancegroupList(req.user, req.query.page);
    }

    @UseGuards(JwtAuthGuard)
    @Get('allowancegroup')
    async getAllowanceGroup(@Request() req): Promise<object> {
        return this.memberservice.getAllowanceGroup(req.user, req.query.id);
    }

    @UseGuards(JwtAuthGuard)
    @Post('createmployee')
    async createUser(@Request() req, @Body() userClass: userClass): Promise<userClass | object> {
        return this.memberservice.createEmployee(userClass, req.user);
    }

    @UseGuards(JwtAuthGuard)
    @Post('createmoderator')
    async createModerator(@Request() req, @Body() userClass: userClass): Promise<userClass | object> {
        return this.memberservice.createModerator(userClass, req.user);
    }

    @UseGuards(JwtAuthGuard)
    @Post('createadmin')
    async createAdmin(@Request() req, @Body() userClass: userClass): Promise<userClass | object> {
        return this.memberservice.createAdmin(userClass, req.user);
    }

    @UseGuards(JwtAuthGuard)
    @Post('createallowancegroup')
    async createAllowanceGroup(@Request() req, @Body() allowancegroupClass: allowancegroupClass): Promise<allowancegroup | object> {
        return this.memberservice.createAllowanceGroup(allowancegroupClass, req.user);
    }

    @UseGuards(JwtAuthGuard)
    @Post('editallowancegroup')
    async editAllowanceGroup(@Request() req, @Body() allowancegroupClass: allowancegroupClass): Promise<allowancegroup | object> {
        return this.memberservice.editAllowanceGroup(allowancegroupClass, req.user);
    }
}
