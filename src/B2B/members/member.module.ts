import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { memberController } from './member.controller';
import { memberService } from './member.service';
import { user, userSchema } from './user.schema';
import { allowancegroup, allowancegroupSchema } from './allowancegroup.schema';
import { company, companySchema } from '../company/company.schema';
import { GlobalServiceService } from '../../global-service/global-service.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: user.name, schema: userSchema }]),
        MongooseModule.forFeature([{ name: allowancegroup.name, schema: allowancegroupSchema }]),
        MongooseModule.forFeature([{ name: company.name, schema: companySchema }]),
    ],
    controllers: [memberController],
    providers: [memberService, GlobalServiceService],
    exports: [memberService],
})
export class memberModule { }