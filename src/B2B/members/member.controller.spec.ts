import { Test, TestingModule } from '@nestjs/testing';
import { memberController } from './member.controller';

describe('member Controller', () => {
  let controller: memberController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [memberController],
    }).compile();

    controller = module.get<memberController>(memberController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
