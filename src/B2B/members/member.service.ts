import { Model } from 'mongoose';
import { Injectable, BadRequestException, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { user, userClass } from './user.schema';
import * as bcrypt from 'bcrypt';
import { allowancegroup, allowancegroupClass } from './allowancegroup.schema';
import { company } from '../company/company.schema';
import { GlobalServiceService } from '../../global-service/global-service.service';
// import { jwtConstants } from '../../auth/constants';

@Injectable()
export class memberService {
    constructor(
        @InjectModel(user.name) private userModel: Model<user>,
        @InjectModel(allowancegroup.name) private allowancegroupModel: Model<allowancegroup>,
        @InjectModel(company.name) private companyModel: Model<company>,
        private readonly global: GlobalServiceService,
    ) {
    }

    async findOne(email1: string): Promise<userClass> {
        return await this.userModel.findOne({ email: email1 }).lean().exec(); // find user by email
    }

    async getProfile(email1: string): Promise<userClass | object> {
        return { data: await this.userModel.findOne({ email: email1 }, '-password', { lean: true }).exec() }; // find user by email but not show password
    }

    async userList(user): Promise<object> {
        console.log("memberService -> user", user)
        if (user.role === 'admin' || user.role === 'moderator') {
            const user1 = await this.userModel.findOne({ _id: user.userId }).exec()
            return { data: await this.userModel.find({ companyId: user1.companyId }).exec(), role: "Admin" };
        } else if (user.role === 'allowance manager') {
            const groupManager = await this.allowancegroupModel.find({ groupadmin: user.userId }).exec()
            let userList = [];
            const dat = groupManager.map(async elem => {
                elem.memberList.map(async user => {
                    const useradd = await this.userModel.findOne({ _id: user }).exec()
                    userList.push(useradd)
                })
            });
            await Promise.all([dat])
            return { data: userList, role: "Manager" };
        }
    }

    async deleteProfile(user, id): Promise<object> {
        if (user.role === 'admin' || user.role === 'moderator') {
            await this.userModel.deleteOne({ _id: id }).exec(); // find user by email but not show password
            return { statusCode: 200, message: "User deleted successfully" }
        }
    }

    async editProfile(data, user): Promise<userClass | object> {
        const user1 = await this.userModel.findOne({ _id: user.userId }).exec();
        if (user1) {
            user1.fullName = data.fullName
            let emailExist = null, phoneExist = null;
            if (!data.groupList) {
                const adminacc = await this.allowancegroupModel.findOne({ groupadmin: user.userId, name: 'Default Group' }).exec()
                // newuser.groupList.push(adminacc._id)
                user1.allowanceGroup = user.groupList
                if (!adminacc.memberList.find(el => { return el === user.groupList })) {
                    adminacc.memberList.push(data._id)
                    adminacc.save();
                }
            } else {
                const adminacc1 = await this.allowancegroupModel.findOne({ _id: user.groupList }).exec()
                // newuser.groupList.push(user.groupList)
                user1.allowanceGroup = user.groupList
                if (!adminacc1.memberList.find(el => { return el === user.groupList })) {
                    adminacc1.memberList.push(user._id)
                    adminacc1.save();
                }
            }
            if (data.email) {
                emailExist = await this.userModel.findOne({ email: data.email, isActive: true }).exec()
            }
            if (data.phone) {
                phoneExist = await this.userModel.findOne({ phone: data.phone, isActive: true }).exec()
            }
            if (emailExist) {
                throw new BadRequestException({ statusCode: 400, message: "This email already exist. Please enter another." })
            } else if (phoneExist) {
                throw new BadRequestException({ statusCode: 400, message: "This phone number already exist. Please enter another." })
            } else {
                user1.phone = data.phone ? data.phone : user1.phone;
                user1.email = data.email ? data.email : user1.email;
                return { data: await user1.save() }
            }
        } else {
            throw new BadRequestException({ statusCode: 400, message: "User not found" })
        }
    }

    async findOneAndChangePassword(data): Promise<userClass | object> {
        const user = await this.userModel.findOne({ _id: data.query.token, isActive: false }).exec()
        if (user) {
            if (data.body.password.length >= 6) {
                const isadminExist = await this.userModel.findOne({ companyId: user.companyId, role: 'admin', isActive: true }).exec() // chek if admin already exist for this company
                if (!isadminExist && user.role === 'admin') {  // Default group created if it's the first admin of the company 
                    const company = await this.companyModel.findOne({ _id: user.companyId }).exec()
                    const allowanceGroup = new this.allowancegroupModel({
                        groupadmin: user._id, name: 'Default Group'
                        , totalWashPerMonth: company.totalNumberWash, companyId: user.companyId, premiumwash: true,
                        addOn: true, createdBy: user._id
                    });
                    allowanceGroup.memberList.push(user._id)   // adding user into allowance group members list
                    user.groupList.push(allowanceGroup._id)    // adding allowance group into user group list
                    await allowanceGroup.save();
                }
                user.password = await bcrypt.hash(data.body.password, 10);
                user.isActive = true;
                return { data: await user.save() };
            } else {
                throw new BadRequestException({ statusCode: 400, message: 'Password should have 6 or greater digits' })
            }
        } else {
            throw new BadRequestException({ statusCode: 400, message: 'User not found.' })
        }
    }

    async createAllowanceGroup(allowancegroup: allowancegroupClass, user): Promise<allowancegroupClass | object> {
        // console.log("adminService -> allowancegroup", allowancegroup, allowancegroup.groupadmin)
        const crearter1 = this.userModel.findOne({ _id: user.userId, isActive: true }).exec()
        const groupAdmin1 = this.userModel.findOne({ email: allowancegroup.groupadmin }).exec();
        const [crearter, groupAdmin] = await Promise.all([crearter1, groupAdmin1])
        if (crearter && crearter.role === 'admin' || crearter.role === 'moderator') { // checking role for creating group
            if (groupAdmin) {
                const remainingSub = await this.allowancegroupModel.findOne({ companyId: crearter.companyId, name: 'Default Group' }).exec();
                if (parseInt(remainingSub.totalWashPerMonth) >= parseInt(allowancegroup.totalWashPerMonth)) { // checking total wash left from default group and update accordingly
                    groupAdmin.role = groupAdmin.role == 'user' ? 'allowance manager' : groupAdmin.role;
                    const allowanceGroup = new this.allowancegroupModel(allowancegroup);
                    allowanceGroup.companyId = crearter.companyId;
                    allowanceGroup.createdBy = crearter._id;
                    allowanceGroup.groupadmin = groupAdmin._id;
                    allowanceGroup.memberList.push(groupAdmin._id)
                    // groupAdmin.groupList.push(allowanceGroup._id)
                    await groupAdmin.save()
                    return { data: await allowanceGroup.save() };
                } else {
                    throw new BadRequestException({ statusCode: 400, message: 'Account total subsciption limit exceeds' });
                }
            } else {
                throw new BadRequestException({ statusCode: 400, message: 'Manager email does not exist' })
            }
        } else {
            throw new UnauthorizedException({ statusCode: 401, message: 'Unauthorized' });
        }
    }

    async editAllowanceGroup(allowancegroup, user): Promise<allowancegroupClass | object> {
        console.log("adminService -> allowancegroup", allowancegroup, allowancegroup.groupadmin)
        const crearter1 = this.userModel.findOne({ _id: user.userId, isActive: true }).exec()
        const groupAdmin1 = this.userModel.findOne({ email: allowancegroup.groupadmin }).exec();
        const allowanceGroup1 = this.allowancegroupModel.findOne({ _id: allowancegroup.id }).exec();
        const [crearter, groupAdmin, allowanceGroup] = await Promise.all([crearter1, groupAdmin1, allowanceGroup1])
        console.log("memberService -> crearter, groupAdmin", crearter, groupAdmin)
        if (crearter && crearter.role === 'admin' || crearter.role === 'moderator' || crearter.role === 'allowance manager') { // checking role for creating group
            if (groupAdmin) {
                if (allowanceGroup) {
                    const remainingSub = await this.allowancegroupModel.findOne({ companyId: crearter.companyId, name: 'Default Group' }).exec();
                    if (parseInt(remainingSub.totalWashPerMonth) >= parseInt(allowancegroup.totalWashPerMonth)) { // checking total wash left from default group and update accordingly
                        groupAdmin.role = groupAdmin.role == 'user' ? 'allowance manager' : groupAdmin.role;
                        allowanceGroup.name = allowancegroup.name;
                        allowanceGroup.addOn = allowancegroup.addOn;
                        allowanceGroup.premiumwash = allowancegroup.premiumwash;
                        allowanceGroup.groupadmin = groupAdmin._id;
                        allowanceGroup.totalWashPerMonth = allowancegroup.totalWashPerMonth;

                        if (!allowanceGroup.memberList.find(el => { return el === groupAdmin._id })) {
                            allowanceGroup.memberList.push(groupAdmin._id)
                        }

                        // if (!groupAdmin.groupList.find(el => { return el === allowanceGroup._id })) {
                        //     groupAdmin.groupList.push(allowanceGroup._id)
                        // }

                        await groupAdmin.save();
                        return { data: await allowanceGroup.save() };
                    } else {
                        throw new BadRequestException({ statusCode: 400, message: 'Account total subsciption limit exceeds should be less than or equal to ' + remainingSub.totalWashPerMonth });
                    }
                } else {
                    throw new BadRequestException({ statusCode: 400, message: 'Allowance group not found' });
                }
            } else {
                throw new BadRequestException({ statusCode: 400, message: 'Email Address not exist' })
            }
        } else {
            throw new UnauthorizedException({ statusCode: 401, message: 'Unauthorized' });
        }
    }

    async createEmployee(user: userClass, admin): Promise<userClass | object> {
        const user1 = await this.userModel.findOne({ _id: admin.userId, isActive: true }).exec()
        if (user1.role === 'admin' || user1.role === 'moderator' || user1.role === 'allowance manager') {
            const userExist = await this.userModel.findOne({ email: user.email, isActive: true }).exec();
            if (userExist) {
                throw new BadRequestException({ statusCode: 400, message: "User Email already exist. Please Enter another one." })
            } else {
                const newuser = new this.userModel(user);
                if (!user.groupList) {
                    const adminacc = await this.allowancegroupModel.findOne({ groupadmin: admin.userId, name: 'Default Group' }).exec()
                    // newuser.groupList.push(adminacc._id)
                    user1.allowanceGroup = user.groupList
                    adminacc.memberList.push(newuser._id)
                    adminacc.save();
                } else {
                    const adminacc1 = await this.allowancegroupModel.findOne({ _id: user.groupList }).exec()
                    // newuser.groupList.push(user.groupList)
                    adminacc1.memberList.push(newuser._id)
                    user1.allowanceGroup = user.groupList
                    adminacc1.save();
                }
                newuser.companyId = user1.companyId
                newuser.createdBy = admin.userId
                newuser.role = 'user';
                const url = `${this.global.basePath}setpassword?id=${newuser._id}`
                this.global.sendEmail(newuser.email, 'Create password for carwash Employee account', 'Click on this link to generate your password for carwash admin link ' + url);
                return { data: await newuser.save() };
            }
        } else {
            throw new UnauthorizedException({ statusCode: 401, message: 'Unauthorized' });
        }
    }

    async createAdmin(user: userClass, admin): Promise<userClass | object> {
        const user1 = await this.userModel.findOne({ _id: admin.userId }).exec()
        if (user1.role === 'admin') {
            const userExist = await this.userModel.findOne({ email: user.email, isActive: true }).exec();
            if (userExist) {
                throw new BadRequestException({ statusCode: 400, message: "User Email already exist. Please Enter another one." })
            } else {
                const newuser = new this.userModel(user);
                newuser.role = 'admin';
                newuser.companyId = user1.companyId;
                newuser.createdBy = admin.userId;
                const url = `${this.global.basePath}setpassword?id=${newuser._id}`
                this.global.sendEmail(newuser.email, 'Create password for carwash Moderator account', 'Click on this link to generate your password for carwash admin link ' + url);
                return { data: await newuser.save() };
            }
        } else {
            throw new UnauthorizedException({ statusCode: 401, message: 'Unauthorized' });
        }
    }

    async createModerator(user: userClass, admin): Promise<userClass | object> {
        const user1 = await this.userModel.findOne({ _id: admin.userId }).exec()
        if (user1.role === 'admin' || user1.role === 'moderator') {
            const userExist = await this.userModel.findOne({ email: user.email, isActive: true }).exec();
            if (userExist) {
                throw new BadRequestException({ statusCode: 400, message: "User Email already exist. Please Enter another one." })
            } else {
                const newuser = new this.userModel(user);
                newuser.role = 'moderator';
                newuser.companyId = user1.companyId;
                newuser.createdBy = admin.userId;
                const url = `${this.global.basePath}setpassword?id=${newuser._id}`
                this.global.sendEmail(newuser.email, 'Create password for carwash Moderator account', 'Click on this link to generate your password for carwash admin link ' + url);
                return { data: await newuser.save() };
            }
        } else {
            throw new UnauthorizedException({ statusCode: 401, message: 'Unauthorized' });
        }
    }

    async allowancegroupList(user, page): Promise<object> {
        const user1 = await this.userModel.findOne({ _id: user.userId }).exec()
        let skipValue = 0;
        if (page != undefined && page > 1) {
            skipValue = (20 * parseInt(page)) - 20;
        }
        if (user.role === 'admin' || user.role === 'moderator') {
            const total = await this.allowancegroupModel.countDocuments({ companyId: user1.companyId });
            return {
                count: total,
                totalPage: Math.ceil(total / 20),
                data: await this.allowancegroupModel.find({ companyId: user1.companyId }).populate('groupadmin', 'fullName email')
                    .limit(20).skip(skipValue).exec()
            }
        } else {
            const total = await this.allowancegroupModel.countDocuments({ $or: [{ createdBy: user.userId }, { groupadmin: user.userId }, { memberList: user.userId }] });
            const totalGroup = await this.allowancegroupModel.find({ $or: [{ createdBy: user.userId }, { groupadmin: user.userId }, { memberList: user.userId }] }).populate('groupadmin', 'fullName email')
                .limit(20).skip(skipValue).exec()
            const dat = totalGroup.map(el => {
                if (el.memberList.indexOf(user.userId)) {
                    el.role = 'Manager';
                    return el;
                }
            })
            return {
                count: total,
                totalPage: Math.ceil(total / 20),
                data: await dat
            }
        }
    }

    async getAllowanceGroup(adminId, id): Promise<allowancegroupClass[] | object> {
        return { data: await this.allowancegroupModel.findOne({ _id: id }).populate('groupadmin', 'fullName email').exec() };
    }

}
