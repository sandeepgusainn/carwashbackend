import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
    timestamps: true
})
export class user extends Document {

    @Prop({})
    fullName: string;

    @Prop({ required: true })
    email: string;

    @Prop({})
    phone: string;

    @Prop({ ref: 'company' })
    companyId: string;

    @Prop({ ref: 'user' })
    createdBy: string;

    @Prop({ required: true, enum: ['admin', 'user', 'allowance manager', 'moderator'] })
    role: string;

    @Prop({ ref: 'allowancegroup' })
    groupList: [];

    @Prop({ ref: 'allowancegroup' })
    allowanceGroup: string;

    @Prop()
    password: string;

    @Prop({ default: false })
    isActive: boolean;
}

export const userSchema = SchemaFactory.createForClass(user);

export class userClass {
    readonly _id: string;
    readonly fullName: string;
    readonly email: string;
    readonly phone: string;
    readonly password: string;
    readonly role: string;
    readonly companyId: string;
    readonly createdBy: string;
    readonly isActive: boolean;
    readonly groupList;
}