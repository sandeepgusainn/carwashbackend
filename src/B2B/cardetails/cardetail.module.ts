import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { cardetailController } from './cardetail.controller';
import { cardetailService } from './cardetail.service';
import { cardetail, cardetailSchema } from './cardetail.schema';
import { user, userSchema } from '../members/user.schema';
import { allowancegroup, allowancegroupSchema } from '../members/allowancegroup.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: cardetail.name, schema: cardetailSchema }]),
        MongooseModule.forFeature([{ name: user.name, schema: userSchema }]),
        MongooseModule.forFeature([{ name: allowancegroup.name, schema: allowancegroupSchema }]),
    ],
    controllers: [cardetailController],
    providers: [cardetailService],
    exports: [cardetailService],
})
export class cardetailModule { }