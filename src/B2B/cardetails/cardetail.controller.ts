/* eslint-disable @typescript-eslint/no-unused-vars */
import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { cardetailService } from './cardetail.service';
import { cardetail } from './cardetail.schema';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';

@Controller('cardetail')
export class cardetailController {
    constructor(
        private readonly cardetailservice: cardetailService,
    ) { }


    @UseGuards(JwtAuthGuard)
    @Post()
    async addCarDetils(@Body() body, @Request() req): Promise<cardetail | object> {
        return this.cardetailservice.addCarDetils(body, req.user);
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    async getCarDetil(@Request() req): Promise<object> {
        console.log(req.user);
        return this.cardetailservice.getCarDetil(req.body.id);
    }

    @UseGuards(JwtAuthGuard)
    @Get('carlist')
    async getAllCars(@Request() req): Promise<object> {
        console.log(req.user);
        return this.cardetailservice.getmyCarDetils(req.user.userId);
    }

    @UseGuards(JwtAuthGuard)
    @Get('allcarlist')
    async getallCarDetil(@Request() req): Promise<object> {
        console.log(req.user);
        return this.cardetailservice.getallCarDetils(req.user);
    }
}
