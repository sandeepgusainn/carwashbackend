import { Test, TestingModule } from '@nestjs/testing';
import { cardetailController } from './cardetail.controller';

describe('cardetail Controller', () => {
  let controller: cardetailController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [cardetailController],
    }).compile();

    controller = module.get<cardetailController>(cardetailController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
