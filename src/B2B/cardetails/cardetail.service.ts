import { Model } from 'mongoose';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { cardetail, cardetailClass } from './cardetail.schema';
import { user } from '../members/user.schema';
import { allowancegroup } from '../members/allowancegroup.schema';

@Injectable()
export class cardetailService {
    constructor(
        @InjectModel(cardetail.name) private carModel: Model<cardetail>,
        @InjectModel(user.name) private userModel: Model<user>,
        @InjectModel(allowancegroup.name) private allowancegroupModel: Model<allowancegroup>,
    ) {
    }
    async addCarDetils(cardetail, user): Promise<cardetail | object> {
        if (cardetail.carAddedType == 'Managed' && user.role != 'user') {
            const cardetails = new this.carModel(cardetail);
            cardetails.createdBy = user.userId;

            const user1 = await this.userModel.findOne({ _id: user.userId }).exec();
            cardetails.companyId = user1.companyId; // if created by other than comapnay than get comapany id from that user
            if (cardetail.carAddedType == 'Managed') {
                const newowner = new this.userModel({   // if added car managed and owned by different person then user for owner is created and that manger will be saved as manager
                    email: cardetail.owneremail, fullName: cardetail.ownername
                    , companyId: cardetails.companyId, createdBy: user.userId, role: 'user'
                })
                await newowner.save()
                cardetails.carOwner = newowner._id
                cardetails.manager = user.userId
            } else {
                cardetails.carOwner = user.userId
            }
            return { data: await cardetails.save()};
        } else {
            throw new BadRequestException({ statusCode: 400, message: "Employee can ony add personal cars." })
        }
    }

    async getmyCarDetils(userId): Promise<object> {
        return { data: await this.carModel.find({ createdBy: userId, carAddedType: 'Personal' }).exec()};
    }

    async getallCarDetils(user): Promise<object> {
        if (user.role === 'admin' || user.role === 'moderator') {
            const user1 = await this.userModel.findOne({ _id: user.userId }).exec()
            return this.carModel.find({ companyId: user1.companyId }).exec();
        } else if (user.role === 'allowance manager') {
            const groupManager = await this.allowancegroupModel.findOne({ groupadmin: user.userId }).exec()
            let carList = [];
            const dat = groupManager.memberList.map(async elem => {
                const usercar = await this.carModel.find({ $or: [{ user: elem }, { createdBy: elem }] }).exec()
                carList.push(usercar)
            });
            await Promise.all([dat])
            return carList;
        } else {
            return { data: await this.carModel.find({ $or: [{ user: user.userId }, { createdBy: user.userId }] }).exec()};
        }
    }

    async getCarDetil(id): Promise<object> {
        return { data: await this.carModel.findOne({ _id: id }).exec()};
    }
}
