import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
    timestamps: true
})
export class cardetail extends Document {

    @Prop({ ref: 'user' })
    createdBy: string;

    @Prop({ ref: 'user' })
    manager: string;

    @Prop({ ref: 'company' })
    companyId: string;

    @Prop({ required: true })
    carModel: string;

    @Prop({ required: true })
    color: string;

    @Prop({})
    carBrand: string;

    @Prop({ required: true })
    carAddedType: string;

    @Prop({ ref: 'user' })
    carOwner: string;

    @Prop()
    LicenseNumber: string;

    @Prop({ default: true })
    isActive: boolean;
}

export const cardetailSchema = SchemaFactory.createForClass(cardetail);

export class cardetailClass {
    readonly _id: string;
    readonly createdBy: string;
    readonly manager: string;
    readonly admin: string;
    readonly carModel: object;
    readonly isActive: boolean;
    readonly carBrand: string;
    readonly carAddedType: string;
    readonly carOwner: string;
    readonly color: string;
    readonly LicenseNumber: string;
}