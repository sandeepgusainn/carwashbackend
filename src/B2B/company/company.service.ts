import { Model } from 'mongoose';
import { Injectable, BadRequestException, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { company, companyClass } from './company.schema';
import { GlobalServiceService } from '../../global-service/global-service.service';
import { user } from '../members/user.schema';
import { request } from '../washrequest/request.schema';
import { addon } from '../addon/addon.schema';
import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class companyService {
    constructor(
        @InjectModel(company.name) private companyModel: Model<company>,
        @InjectModel(user.name) private userModel: Model<user>,
        @InjectModel(request.name) private requestModel: Model<request>,
        @InjectModel(addon.name) private addonModel: Model<addon>,
        private readonly global: GlobalServiceService,
    ) {
    }

    async create(company): Promise<companyClass | object> {
        const companyExist1 = this.companyModel.findOne({ email: company.email, isActive: true }).exec();
        const userExist1 = this.userModel.findOne({ email: company.email, isActive: true }).exec();
        const [companyExist, userExist] = await Promise.all([companyExist1, userExist1])
        if (companyExist || userExist) {
            throw new BadRequestException({ statusCode: 400, message: "Email already exist. Please Enter another one." })
        } else {
            const createdCat = new this.companyModel(company);
            const newowner = new this.userModel({   // if added car managed and owned by different person then user for owner is created and that manger will be saved as manager
                email: company.email, companyId: createdCat._id, role: 'admin',
                fullName: company.adminName ? company.adminName : company.name
            })

            const customerId1 = this.global.createCustomer(company);
            const totalNumberWash1 = this.global.getNumberWash(company.productId);
            const [customerId, totalNumberWash] = await Promise.all([customerId1, totalNumberWash1, await newowner.save()])
            createdCat.customerId = customerId.id;
            createdCat.totalNumberWash = totalNumberWash.transform_quantity.divide_by;
            createdCat.priceId = totalNumberWash.id;
            console.log("companyService -> createdCat", createdCat)
            const url = `${this.global.basePath}setpassword?id=${newowner._id}`
            this.global.sendEmail(newowner.email, 'Create password for carwash admin account', 'Click on this link to generate your password for carwash admin link ' + url);
            createdCat.isActive = true;
            return createdCat.save();
        }
    }

    async findAll(): Promise<object> {
        return { data: await this.companyModel.find().exec()};
    }

    async findOne(email: string): Promise<object> {
        // console.log("adminService -> constructor -> email1", adminId)
        return { data: await this.companyModel.findOne({ email: email }).exec()};
    }

    async getInvoice(user): Promise<string | object> {
        // console.log("adminService -> constructor -> email1", adminId)
        if (user.role === 'admin') {
            const today = moment().format();
            const lastMonthDate = moment().subtract(1, 'months').format();
            console.log("companyService -> lastMonthDate", today, lastMonthDate)
            const company1 = this.companyModel.findOne({ email: user.email }).exec()
            const requestLastMonth1 = this.requestModel.find({
                requestStatus: 'Completed', createdAt: {
                    $lte: today,
                    $gte: lastMonthDate
                }
            }).exec()
            const [company, requestLastMonth] = await Promise.all([company1, requestLastMonth1])
            if (requestLastMonth) {
                const totalWashThisMonth = requestLastMonth.length
                const extraWash = company.totalNumberWash >= totalWashThisMonth ? 0 : totalWashThisMonth - company.totalNumberWash;
                const extraWashprice = extraWash > 0 ? await this.global.getNumberWash(company.productId) : null;  // need extrawash price details
                const allAddon = _.flatMap(requestLastMonth, function (o) { return o.addons; });
                // console.log("companyService -> allAddon", allAddon, extraWashprice, extraWash, totalWashThisMonth)
                let addonList = [];
                const dat = allAddon.map(async element => {
                    const addon = await this.addonModel.findOne({ _id: element.id })  // get addon detail for price ids of stripe billing
                    const isAddonExist = addonList.find(v => v.addonPrice == addon.priceId);
                    // console.log("companyService -> isAddonExist", isAddonExist)
                    if (isAddonExist) {
                        isAddonExist.addonQuatity += 1
                    } else {
                        addonList.push({ addonQuatity: 1, addonPrice: addon.priceId })
                    }
                });
                await Promise.all(dat)

                const data = {
                    subPrice: company.priceId,
                    addon: addonList,
                    extraWashQuatity: extraWash,
                    extraWashPrice: extraWashprice
                }

                // console.log("companyService -> requestLastMonth", requestLastMonth, addonList)
                return { data: await this.global.createInvoice(company.customerId, data)};
            } else {
                console.log("No completed car wash request last month");
                throw new BadRequestException({ statusCode: 400, message: "No completed car wash request last month" })
            }
        } else {
            throw new UnauthorizedException({ statusCode: 401, message: 'Unauthorized' })
        }
    }

}
