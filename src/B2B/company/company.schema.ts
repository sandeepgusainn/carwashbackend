import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
    timestamps: true
})
export class company extends Document {

    @Prop({ required: true })
    email: string;

    @Prop({ required: true })
    name: string;

    @Prop({ ref: 'product' })
    productId: string;

    @Prop({})
    priceId: string;

    @Prop({ required: true })
    vatNumber: string;

    @Prop({ required: true })
    totalNumberWash: number;

    @Prop({})
    customerId: string;

    @Prop({})
    companyDetails: Object;

    @Prop({ default: true })
    isActive: boolean;

}

export const companySchema = SchemaFactory.createForClass(company);

export class companyClass {
    readonly _id: string;
    readonly name: string;
    readonly productId: string;
    readonly priceId: string;
    readonly vatNumber: string;
    readonly companyDetails;
    readonly isActive: boolean;
    readonly email: string;
    readonly customerId: string;
    readonly totalNumberWash: number;
}