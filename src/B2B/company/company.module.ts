import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { companyController } from './company.controller';
import { companyService } from './company.service';
import { company, companySchema } from './company.schema';
import { GlobalServiceService } from '../../global-service/global-service.service';
import { jwtConstants } from '../../auth/constants';
import { JwtModule } from '@nestjs/jwt';
import { allowancegroup, allowancegroupSchema } from '../members/allowancegroup.schema';
import { user, userSchema } from '../members/user.schema';
import { request, requestSchema } from '../washrequest/request.schema';
import { addon, addonSchema } from '../addon/addon.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: company.name, schema: companySchema }]),
        MongooseModule.forFeature([{ name: allowancegroup.name, schema: allowancegroupSchema }]),
        MongooseModule.forFeature([{ name: user.name, schema: userSchema }]),
        MongooseModule.forFeature([{ name: request.name, schema: requestSchema }]),
        MongooseModule.forFeature([{ name: addon.name, schema: addonSchema }]),
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: { expiresIn: '1h' },
        }),
    ],
    controllers: [companyController],
    providers: [companyService, GlobalServiceService],
    exports: [companyService],
})
export class companyModule { }