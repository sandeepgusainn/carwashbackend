import { Test, TestingModule } from '@nestjs/testing';
import { companyController } from './company.controller';

describe('company Controller', () => {
  let controller: companyController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [companyController],
    }).compile();

    controller = module.get<companyController>(companyController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
