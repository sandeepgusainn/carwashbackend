/* eslint-disable @typescript-eslint/no-unused-vars */
import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { companyService } from './company.service';
import { company, companyClass } from './company.schema'
import { userClass } from '../members/user.schema'
import { allowancegroup, allowancegroupClass } from '../members/allowancegroup.schema'
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';

@Controller('company')
export class companyController {
    constructor(
        private readonly companyservice: companyService,
    ) { }

    @Post('signup')
    async create(@Body() companyClass: companyClass): Promise<companyClass | object> {
        return this.companyservice.create(companyClass);
    }

    @UseGuards(JwtAuthGuard)
    @Get('detail')
    async getProfile(@Request() req): Promise<object> {
        console.log(req.user);
        return this.companyservice.findOne(req.user.email);
    }

    @UseGuards(JwtAuthGuard)
    @Get('invoice')
    async getInvoice(@Request() req): Promise<string | object> {
        console.log(req.user);
        return this.companyservice.getInvoice(req.user);
    }

}
