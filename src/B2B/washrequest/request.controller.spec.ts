import { Test, TestingModule } from '@nestjs/testing';
import { requestController } from './request.controller';

describe('request Controller', () => {
  let controller: requestController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [requestController],
    }).compile();

    controller = module.get<requestController>(requestController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
