/* eslint-disable @typescript-eslint/no-unused-vars */
import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { requestService } from './request.service';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';

@Controller('request')
export class requestController {
    constructor(
        private readonly requestservice: requestService,
    ) { }

    @UseGuards(JwtAuthGuard)
    @Post()
    async CreateWashRequest(@Request() req): Promise<object | object> {
        return this.requestservice.createWashRequest(req.body, req.user)
    }

    @UseGuards(JwtAuthGuard)
    @Post('edit')
    async editWashRequest(@Request() req): Promise<object> {
        return this.requestservice.editWashRequest(req.body)
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    async getRequest(@Request() req): Promise<object> {
        return this.requestservice.findOne(req.query.id);
    }

    @UseGuards(JwtAuthGuard)
    @Post('cancel')
    async cancelRequest(@Request() req): Promise<object> {
        return this.requestservice.cancel(req.body.id, req.user);
    }

    @UseGuards(JwtAuthGuard)
    @Get('futurewash')
    async getFutureRequest(@Request() req): Promise<object> {
        return this.requestservice.findFuture(req.user.userId);
    }

    @UseGuards(JwtAuthGuard)
    @Get('pastwash')
    async getPastRequest(@Request() req): Promise<object> {
        return this.requestservice.findPast(req.user.userId);
    }

    @UseGuards(JwtAuthGuard)
    @Get('all')
    async getAllRequest(@Request() req): Promise<object> {
        return this.requestservice.findAll(req.user.userId);
    }

}
