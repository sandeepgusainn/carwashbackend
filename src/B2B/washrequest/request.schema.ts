import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
    timestamps: true
})
export class request extends Document {
    @Prop({ ref: 'user' })
    createdBy: string;

    @Prop({ ref: 'user' })
    carOwner: string;

    @Prop({ ref: 'company' })
    companyId: string;

    @Prop({ ref: 'cardetail' })
    car: [];

    @Prop({ ref: 'addon' })
    addons: [];

    @Prop({ required: true })
    washInstructions: string;

    @Prop({ default: true })
    isActive: boolean;
   
    @Prop({ default: true })
    premiumwash: boolean;

    @Prop({})
    requestStatus: string;

    @Prop({})
    requestDate: string;

    @Prop({})
    requestDateTime: string;

    @Prop({})
    requestTime: string;

    @Prop({ ref: 'address' })
    address: string;
}

export const requestSchema = SchemaFactory.createForClass(request);

export class requestClass {
    readonly _id: string;
    readonly createdBy: string;
    readonly carOwner: string;
    readonly admin: string;
    readonly car;
    readonly isActive: boolean;
    readonly addons;
    readonly washInstructions: string;
    readonly address: string;
    readonly requestStatus: string;
    readonly requestDateTime: string;
    readonly requestTime: string;
    readonly requestDate: string;
}