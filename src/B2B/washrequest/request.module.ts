import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { requestController } from './request.controller';
import { requestService } from './request.service';
import { request, requestSchema } from './request.schema';
// import { allowancegroup, allowancegroupSchema } from '../members/allowancegroup.schema';
import { user, userSchema } from '../members/user.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: request.name, schema: requestSchema }]),
        MongooseModule.forFeature([{ name: user.name, schema: userSchema }]),
    ],
    controllers: [requestController],
    providers: [requestService],
    exports: [requestService],
})
export class requestModule { }