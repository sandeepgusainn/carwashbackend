import { Model } from 'mongoose';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { request, requestClass } from './request.schema';
import { user } from '../members/user.schema';
// import { GlobalServiceService } from '../../global-service/global-service.service';
import * as moment from 'moment';

@Injectable()
export class requestService {
    constructor(
        @InjectModel(request.name) private requestModel: Model<request>,
        @InjectModel(user.name) private userModel: Model<user>,
        // private readonly global: GlobalServiceService,
    ) {
    }

    async createWashRequest(data: requestClass, user): Promise<requestClass | object> {
        const time = data.requestTime.split(':');
        const requestDateTime = moment(data.requestDate).hour(parseInt(time[0])).minute(parseInt(time[1]))
        console.log(moment().add(1, 'days').startOf('day'), moment(data.requestDate), requestDateTime)
        if (moment().add(1, 'days').startOf('day') <= moment(data.requestDate)) {
            const request = new this.requestModel(data);
            request.addons = []
            console.log("requestService -> request", request)

            data.addons.forEach(element => {
                console.log("requestService -> element", element)
                request.addons.push(element)
            });

            console.log("requestService -> element", request)

            const user1 = await this.userModel.findOne({ _id: user.userId }).exec();
            console.log("requestService -> user1", user1)
            request.createdBy = user1._id;
            request.companyId = user1.companyId;
            request.requestStatus = 'Pending'
            request.requestDateTime = requestDateTime
            return { data: await request.save()};
        } else {
            throw new BadRequestException({ statusCode: 400, message: "Choose one day later date." })
        }
    }

    async editWashRequest(data: requestClass): Promise<requestClass | object> {
        const request = await this.requestModel.findOne({ _id: data._id }).exec();
        request.addons = []
        console.log("requestService -> request", request)

        data.addons.forEach(element => {
            console.log("requestService -> element", element)
            request.addons.push(element)
        });
        request.requestStatus = 'Pending'
        const time = data.requestTime.split(':');
        request.requestDateTime = moment(data.requestDate).hour(parseInt(time[0])).minute(parseInt(time[1]))
        return { data: await request.save()};
    }

    async findOne(id: string): Promise<requestClass | object> {
        return { data: await this.requestModel.findOne({ _id: id }).exec()};
    }

    async cancel(id: string, userId: string): Promise<requestClass | object> {
        return { data: await this.requestModel.updateOne({
            $or: [
                { carOwner: userId, _id: id },
                { createdBy: userId, _id: id }
            ]
        }, { $set: { requestStatus: 'Cancel' } }).exec()};
    }

    async findFuture(userId: string): Promise<requestClass[] | object> {
        return { data: await this.requestModel.find({
            $or: [{ carOwner: userId, requestDateTime: { "$gte": moment().format() } },
            { createdBy: userId, requestDateTime: { "$gte": moment().format() } }]
        }).populate('carOwner').populate('car').populate('address').exec()};
    }

    async findPast(userId: string): Promise<requestClass[] | object> {
        return { data: await this.requestModel.find({
            $or: [{ carOwner: userId, requestDateTime: { "$lte": moment().format() } },
            { createdBy: userId, requestDateTime: { "$lte": moment().format() } }]
        }).populate('carOwner').populate('car').populate('address').exec()};
    }

    async findAll(userId: string): Promise<requestClass[] | object> {
        return { data: await this.requestModel.find({ $or: [{ carOwner: userId }, { createdBy: userId }] })
            .populate('carOwner').populate('car').populate('address').exec()};
    }
}
