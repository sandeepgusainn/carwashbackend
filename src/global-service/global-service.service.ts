import { Injectable } from '@nestjs/common';
import * as nodemailer from "nodemailer";
import Stripe from 'stripe';

@Injectable()
export class GlobalServiceService {

    basePath = 'https://carfront.dontpanic.in/';
    private readonly stripeKey = 'sk_test_51GvKFfDVr6LDsNjVJOdWKQU7oZpTSUqFVAM2yTxLAJ9WQG6Tda0WvulLJ0nK8wULYK6zCMAcwwYHmz9py4mwJKMD00sTjyTdTF';
    private readonly stripe = new Stripe(this.stripeKey, {
        apiVersion: '2020-03-02',
        typescript: true
    });

    async createCustomer(data): Promise<any> {
        const params: Stripe.CustomerCreateParams = {
            name: data.name,
            email: data.email
        };

        return this.stripe.customers.create(params);
    }

    async getProductList(): Promise<any> {
        return this.stripe.products.list();
    }

    async getNumberWash(productId: string): Promise<any> {
        const list = await this.stripe.prices.list();
        return list.data.find(val => {
            return val.product === productId
        })
    }

    async getPricesList(): Promise<any> {
        return this.stripe.prices.list();
    }

    async createInvoice(customer, data): Promise<string> {
        console.log("GlobalServiceService -> data", data)
        const invoiceItem = await this.stripe.invoiceItems.create({   //  Subscription product deatils added in invoice
            customer: customer,
            price: data.subPrice,
            period: {
                start: Date.now(),
                end: Date.now() + 1,
            }
        });

        if (data.addon.length > 0) {
            let dat = data.addon.map(async element => {
                await this.stripe.invoiceItems.create({   // addon product deatils added in invoice
                    customer: customer,
                    quantity: element.addonQuatity,
                    price: element.addonPrice,
                    period: {
                        start: Date.now(),
                        end: Date.now() + 1,
                    }
                });
            });
            await Promise.all(dat)
        }

        if (data.extraWashQuatity > 0) {
            const invoiceItem2 = await this.stripe.invoiceItems.create({   // Extra wash product deatils added in invoice
                customer: customer,
                quantity: data.extraWashQuatity,
                price: data.extraWashPrice,
                period: {
                    start: Date.now(),
                    end: Date.now() + 1,
                }
            });
        }
        const allitem = await this.stripe.invoiceItems.list();
        console.log("GlobalServiceService -> allitem", allitem)
        
        const invoice = await this.stripe.invoices.create({
            customer: customer,
            collection_method: 'send_invoice',
            days_until_due: 10,
            auto_advance: true, // auto-finalize this draft after ~1 hour
        });
        return 'Invoice sent'
    }

    async sendEmail(to, subject, contents): Promise<void> {

        const smtpTransport = nodemailer.createTransport({
            service: 'gmail',
            tls: {
                rejectUnauthorized: false
            },
            auth: {
                user: 'dontpanic.in@gmail.com',
                pass: '1@DontPanic.in1'
            }
        });
        const mailOptions = {
            to: to,
            from: 'dontpanic.in@gmail.com',
            subject: subject,
            text: contents
        };
        await smtpTransport.sendMail(mailOptions);
        return;
    };

}
