import { Injectable } from '@nestjs/common';
import { memberService } from '../B2B/members/member.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { InjectModel } from '@nestjs/mongoose';
import { company } from '../B2B/company/company.schema';
import { Model } from 'mongoose';

@Injectable()
export class AuthService {
    constructor(
        private readonly memberservice: memberService,
        private readonly jwtService: JwtService,
        @InjectModel(company.name) private companyModel: Model<company>,
    ) { }

    async validateUser(email: string, pass: string): Promise<any> {
        const user = await this.memberservice.findOne(email);
        if (user && await bcrypt.compare(pass, user.password)) {
            return user;
        } else {
            return null;
        }
    }

    async login(user) {
        const payload = { email: user.email, sub: user._id, role: user.role };
        delete user.password;
        const comapnay = await this.companyModel.findOne({ _id: user.companyId }).lean();
        console.log("AuthService -> login -> payload", payload, user)
        return {
            user: user,
            totalSubscription: comapnay.totalNumberWash,
            access_token: this.jwtService.sign(payload),
        };
    }
}