import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { memberModule } from '../B2B/members/member.module';
import { AuthService } from './auth.service';
import { jwtConstants } from './constants';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LocalStrategy } from './strategies/local.strategy';
import { company, companySchema } from '../B2B/company/company.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
    imports: [
        memberModule,
        MongooseModule.forFeature([{ name: company.name, schema: companySchema }]),
        PassportModule.register({
            defaultStrategy: 'jwt'
        }),
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: { expiresIn: '48h' },
        }),
    ],
    providers: [AuthService, LocalStrategy, JwtStrategy],
    exports: [AuthService],
})
export class AuthModule { }
